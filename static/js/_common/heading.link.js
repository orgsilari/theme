(
	function addHeadingLinks() {
		const article = document.querySelector('#article');
		if (!article) {
			return;
		}

		const headings = article.querySelectorAll('h1, h2, h3, h4, h5, h6');
		if (headings.length === 0) {
			return;
		}

		for (const heading of headings) {
			if (!heading.className.split(' ').includes('skiplink')) {
				const a = document.createElement('a');
				a.innerHTML = heading.innerHTML;
				a.href = '#' + heading.id;
				heading.innerHTML = '';
				heading.append(a);
			}
		}
	}
)
();
